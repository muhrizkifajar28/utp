import java.util.Scanner;

public class Kasir {
    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    /**
     * Method digunakan untuk menampilkan daftarMeja beserta keterangannya.
     */
    public void tampilkanDaftarMeja() {
        //EDIT DISINI
        for (Meja meja : daftarMeja) {
            if (meja.isKosong()) {
                System.out.printf("Meja %d (kosong)\n",meja.getNomorMeja());
            } else {
                System.out.printf("Meja %d (terisi oleh pelanggan %s)\n",meja.getNomorMeja(),meja.getPelanggan().getNama());
            }
        }
    }

    /**
     * Method digunakan untuk menambah pelanggan pada meja tertentu.
     * @param nomorMeja nomor meja yang ingin ditambahkan pelanggan.
     * @param pelanggan pelanggan yang ingin ditambahkan ke meja.
     */
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja-1];
        //dapat mengisi pelanggan ketika kondisi pelanggan pada meja masih null
        if (meja.isKosong()) {
            meja.setPelanggan(pelanggan);
        } else {
            System.out.println("Meja sudah terisi");
        }
    }
    
    /**
     * Method digunakan untuk menambah pesanan di daftarMenu pada meja tertentu. 
     * @param nomorMeja nomor meja yang ingin ditambahkan pesanan.
     * @param menu pesanan yang ingin ditambahkan.
     */
    public void tambahPesanan(int nomorMeja, Menu menu) {
        //EDIT DISINI
        //isExist digunakan untuk mengecek apakah pesanan tersedia
        boolean isExist = false;

        Meja meja = daftarMeja[nomorMeja-1];
        for (Menu Menu : daftarMenu) {
            if (Menu == menu) {
                isExist = true;
                break;
            }
        }

        //jika pesanan tersedia maka dapat menambahkannya ke daftar menu
        if (isExist) {
            meja.setMenu(menu);
        } else {
            System.out.println("Menu is null");
        }
    }

    /**
     * Method digunakan untuk menghapus pelanggan pada meja tertentu
     * @param nomorMeja nomor meja yang ingin pelanggannya dihapus
     */
    public void hapusPelanggan(int nomorMeja) {
        //EDIT DISINI
        Meja meja = daftarMeja[nomorMeja-1];
        if (meja.isKosong()) {
            System.out.println("Meja belum terisi oleh pelanggan");
        } else {
            meja.setPelanggan(null);
        }
    }

    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga += menu[i].getHarga();
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            System.out.println("masuk");
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    System.out.println("Meja " + nomorMeja + " - " + pelanggan.getNama() + " memesan " + menu[i].getNama() + " seharga " + menu[i].getHarga());
                }
            }
        } else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }    

    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();  
            switch (pilihan) {
                case 1:
                    // menampilkan daftar meja dengan method yang sudah ada
                    //EDIT DISINI
                    tampilkanDaftarMeja();
                    break;
                case 2:
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan pada method
                    // jangan lupa instansiasi Pelanggan dengan nama pelanggan sesuai input
                    // EDIT DISINI
                    System.out.print("Masukkan No Meja : ");
                    int noMeja2 = scanner.nextInt();
                    scanner.nextLine();
                    if (noMeja2>10||noMeja2<0) {
                        System.out.println("meja tidak ada [1-10]");
                        break;
                    }
                    System.out.print("Masukkan Nama : ");
                    String nama2 = scanner.nextLine();

                    Pelanggan pelanggan = new Pelanggan(nama2);
                    tambahPelanggan(noMeja2, pelanggan);
                    break;

                case 3:
                boolean stopLoop = false;
                System.out.print("Masukkan nomor meja: ");
                int nomorMejaPesan = scanner.nextInt();
                Boolean meja = daftarMeja[nomorMejaPesan - 1].isKosong();
                scanner.nextLine();  
                if (!meja) {
                    tampilkanDaftarMenu();
                    while (!stopLoop) {
                        System.out.print("Masukkan nomor menu: ");
                        int nomorMenuPesan = scanner.nextInt();
                        scanner.nextLine();  
                        switch (nomorMenuPesan) {
                            case 1:
                                tambahPesanan(nomorMejaPesan, daftarMenu[0]);
                                break;
                            case 2:
                                tambahPesanan(nomorMejaPesan, daftarMenu[1]);
                                break;
                            case 3:
                                tambahPesanan(nomorMejaPesan, daftarMenu[2]);
                                break;
                            case 4:
                                tambahPesanan(nomorMejaPesan, daftarMenu[3]);
                                break;
                            case 5:
                                tambahPesanan(nomorMejaPesan, daftarMenu[4]);
                                break;
                            case 6:
                                stopLoop = true;
                                break;
                            default:
                                System.out.println("Nomor menu tidak valid");
                                break;
                        }
                    }
                }
                else {
                    System.out.println("Meja tidak ada pelanggan");
                }
                break;
                case 4:
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan pada method hapusPelanggan()
                    // EDIT DISINI
                    System.out.print("Masukkan No Meja : ");
                    int noMeja4 = scanner.nextInt();
                    if (noMeja4>10||noMeja4<0) {
                        System.out.println("meja tidak ada [1-10]");
                        break;
                    }
                    hapusPelanggan(noMeja4);
                    break;
                case 5:
                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun / total harga 0
                    // EDIT DISINI
                    System.out.print("Masukkan No Meja : ");
                    int noMeja5 = scanner.nextInt();
                    if (noMeja5>10||noMeja5<0) {
                        System.out.println("meja tidak ada [1-10]");
                        break;
                    }
                    int total = hitungHargaPesanan(noMeja5);
                    // kondisi ketika meja belum meiliki pesanan
                    if (total == 0) {
                        System.out.println("Meja belum memiliki pesanan");
                    } else {
                        System.out.printf("Harga pesanan pada meja no %d adalah %d\n",noMeja5,total);
                    }
                    break;
                    
                case 6:
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja 
                    // EDIT DISINI
                    System.out.print("Masukkan No Meja : ");
                    int noMeja6 = scanner.nextInt();
                    if (noMeja6>10||noMeja6<0) {
                        System.out.println("meja tidak ada [1-10]");
                        break;
                    }
                    Menu[] menuPadaMeja = daftarMeja[noMeja6].getMenu();
                    tampilkanPesanan(noMeja6);
                    // kondisi ketika meja sudah ada pelanggan namun belum melakukan pemesanan
                    if (menuPadaMeja[0]==null) {
                        System.out.printf("Pelanggan meja %d belum melakukan pemesanan\n",noMeja6);
                    }
                    break;

                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                System.out.println("Pilihan tidak valid");
                break;
        }
        System.out.println();
    }
    scanner.close();
    }
}